# -*- coding: utf-8 -*-
'''
Created on 2014. 11. 17.

@author: ko
'''

import traceback

class TCompanyTrafficIpInfoDao(object):
    
    #
    def __init__(self, connectDbObj, loggerObj):
        
        self.className = self.__class__.__name__
        self.connectDbObj = connectDbObj
        self.loggerObj = loggerObj

    #
    def selectForNfdump(self):
        
        method_name = traceback.extract_stack()[-1][2]
        self.loggerObj.start(self.className, method_name)
        
        collect_cidr_data_list = []
        
        selectQuery = "select nCompanySeq, nContractSeq, sNickName, sCidr from tCompanyTrafficIpInfo where nCollectFlag = 0"
        
        try:
            cursor = self.connectDbObj.cursor()
            
            cursor.execute(selectQuery)
            
            rows = cursor.fetchall()
            
            for row in rows:
                collect_cidr_data = (row[0], row[1], row[2], row[3])
                collect_cidr_data_list.append(collect_cidr_data)
        except:
            raise
        
        self.loggerObj.finish(self.className, method_name, collect_cidr_data_list)
        
        return collect_cidr_data_list
    
    #
    #
    def selectWhere(self, nCompanySeq, nContractSeq=None, sCidr=None):
        
        method_name = traceback.extract_stack()[-1][2]
        self.loggerObj.start(self.className, method_name, nCompanySeq, nContractSeq, sCidr)
        
        target_dict_obj = {}
        
        try:
            selectStr = "select sNickName, sCidr from tCompanyTrafficIpInfo "
            whereStr = ""
            
            if sCidr is not None:
                whereStr = "where nCompanySeq = %s and sCidr = %s" % (nCompanySeq, sCidr)
            elif nContractSeq is not None and sCidr is None:
                whereStr = "where nCompanySeq = %s and nContractSeq = %s" % (nCompanySeq, nContractSeq)
            else:
                whereStr = "where nCompanySeq = %s" % nCompanySeq
    
            selectQuery = str(selectStr) + whereStr
            
            cursor = self.connectDbObj.cursor()
            
            cursor.execute(selectQuery)
            
            rows = cursor.fetchall()
            
            if rows.__len__() != 0:
                target_dict_obj['sNickName'] = rows[0][0]
            
                cidr_list = []    
            
                for row in rows:
                    cidr_list.append(row[1])
                    
                target_dict_obj['cidr_list'] = cidr_list
        except:
            raise
        
        self.loggerObj.finish(self.className, method_name, target_dict_obj)
        
        return target_dict_obj

    