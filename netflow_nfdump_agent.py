# -*- coding: utf-8 -*-
'''
Created on 2014. 11. 14.

@author: ko
'''

from control.nfdump_daemon import NfdumpDaemon
from common.daemon import run_daemon

if __name__ == '__main__':
    
    run_daemon(NfdumpDaemon)