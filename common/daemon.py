# -*- coding: utf-8 -*-
class Daemon(object):
    def __init__(self, conf):
        self.conf = conf

    def run_forever(self):
        raise NotImplementedError('run_forever not implemented')

    def run(self):
        self.run_forever()

def run_daemon(klass):
    try:
        klass().run()
    except KeyboardInterrupt:
        print('User quit')
    print('Exited')

